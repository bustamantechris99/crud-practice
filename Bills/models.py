from django.db import models
from django.conf import settings


class Bill(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='bills', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    due_date = models.DateTimeField(auto_now_add=False)
    price = models.DecimalField(max_digits=7, decimal_places=2, blank=False, null=True)
    paid = models.BooleanField(default=False)


    def __str__(self):
        return self.name
    class Meta:
      ordering= ['paid']