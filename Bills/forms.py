from .models import Bill
from django.forms import ModelForm
from django import forms


class BillForm(ModelForm):
    class Meta:
      model = Bill
      fields = ['name', 'due_date', 'price', 'paid']

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder':'username', 'size':'80'}))
    password = forms.CharField(max_length=150, widget=forms.PasswordInput(attrs={'placeholder':'password', 'size':'80'}))


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder':'username', 'size':'80'}))
    password =forms.CharField(max_length=150, widget=forms.PasswordInput(attrs={'placeholder':'password','size':'80'}))
    password_confirmation = forms.CharField(max_length=150, widget=forms.PasswordInput(attrs={'placeholder':'password-confirmation', 'size':'80'}))