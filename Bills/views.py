from django.shortcuts import render, redirect, get_object_or_404
from django.utils.timezone import datetime
from .models import Bill
from .forms import BillForm, LoginForm, SignUpForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

@login_required(login_url='http://127.0.0.1:8000/login/')
def display(request):
    bills = Bill.objects.filter(user = request.user)
    today = datetime.today()
    print(today)
    context = {
      'bills':bills,
      'todaysdate':today
    }
    return render(request, 'Bills/display_bills.html', context)

@login_required(login_url='http://127.0.0.1:8000/login/')
def add_bill(request):
    if request.method == 'POST':
        form = BillForm(request.POST)
        if form.is_valid():
            bill = form.save(False)
            bill.user = request.user
            bill.save()
            return redirect('main')
    else:
        form = BillForm()
    context = {
      'form':form
    }
    return render(request, 'Bills/add_bill.html', context)


@login_required(login_url='http://127.0.0.1:8000/login/')
def delete_bill(request, id):
    bill = get_object_or_404(Bill, id=id)
    context = {
      'bill': bill
    }
    if request.method == 'GET':
        return render(request, 'Bills/delete_bill.html', context)
    else:
        bill.delete()
        return redirect('main')

@login_required(login_url='http://127.0.0.1:8000/login/')
def edit_bill(request, id):
    bill = get_object_or_404(Bill, id=id)
    if request.method == 'POST':
        form = BillForm(request.POST, instance = bill)
        if form.is_valid():
            new = form.save(False)
            new.user = request.user
            new.save()
            return redirect('main')
    else:
        form = BillForm(instance = bill)
    context = {
      'form':form
    }
    return render(request, 'Bills/update_bill.html', context)


def Login_user(request):
    if request.method =='POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('main')
    else:
        form = LoginForm()
    context = {
      'form':form
    }
    return render(request, 'Bills/login.html', context)

@login_required(login_url='http://127.0.0.1:8000/login/')
def Logout(request):
    logout(request)
    return redirect('login')


def RegisterUser(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(username=username, email=None, password=password)
                login(request, user)
                return redirect('main')
            else:
              form.add_error('password', 'Passwords do not match!')

    else:
        form = SignUpForm()
    context = {
      'form': form
    }
    return render(request, 'Bills/register.html', context)