from django.urls import path
from .views import display, add_bill, delete_bill, edit_bill, Login_user, Logout, RegisterUser


urlpatterns = [
    path('main/', display, name='main'),
    path('addbill/', add_bill, name='add_bill'),
    path('deletebill/<int:id>/', delete_bill, name='delete_bill'),
    path('updatebill/<int:id>/', edit_bill, name='update'),
    path('login/', Login_user, name='login'),
    path('logout/', Logout, name='logout'),
    path('register/', RegisterUser, name='register')
]
